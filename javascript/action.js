        
        

        // Previsualisation de l'image ******************************************************************************************
        var loadFile = function(event) {
        var profil = document.getElementById('pp');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };

        // traitement du nom  ***************************************************************************************************
                
        var     nom = document.getElementById('nom'),
                alertnom = document.getElementById('alertnom');

        nom.addEventListener('focus', function(){       // on se rassure que les erreurs ne s'affichent pas lorsqu'on est dans le champ.
                alertnom.style.opacity='0';
        });

        nom.addEventListener('blur', testnom);  // on controle la valeur laissee dans le champ
        function testnom(){
                // console.log(nom.value);  //verification du fonctionnement
                if(nom.value.length<3){  //test de taille de contenu
                        alertnom.innerHTML = "Le nom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
                        alertnom.style.opacity='1';   //penser a le faire avec une duree definie. 
                        return false;
                }
                else if(contient_specialchart(nom.value)){  // test de caracteres non indiques
                        alertnom.innerHTML = "Le nom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
                        alertnom.style.opacity = '1';
                        return false;
                }else{
                        return true;
                }
        }
        function contient_specialchart(char){
                var special = ['@','!','~','<','>','?',' " '," ' ",'*','(',')','^','%','$','#','&','{','}','[',']',';',];

                for (var i = special.length - 1; i >= 0; i--) { //pour chaque element de special
                        for (var id in char) {  // on se rassure qu'il ne soit pas dans char
                                if(char[id]==special[i]){
                                        return true;
                                }
                        }
                }
                return false;
        }

        // traitement du prenom ************************************************************************
        var     prenom = document.getElementById('prenom'),
                alertprenom = document.getElementById('alertprenom');

        prenom.addEventListener('focus', function(){    // on se rassure que les erreurs ne s'affichent pas lorsqu'on est dans le champ.
                alertprenom.style.opacity='0';
        });

        prenom.addEventListener('blur', testprenom);  // on controle la valeur laissee dans le champ
        function testprenom(){
                // console.log(nom.value);  //verification du fonctionnement
                if(prenom.value.length<3){  //test de taille de contenu
                        alertprenom.innerHTML = "Le prenom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
                        alertprenom.style.opacity='1';   //penser a le faire avec une duree definie. 
                        return false;
                }
                else if(contient_specialchart(prenom.value)){  // test de caracteres non indiques
                        alertprenom.innerHTML = "Le prenom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
                        alertprenom.style.opacity = '1';
                        return false;
                }else{
                        return true;
                }
        }

        // traitement du phone number ***********************************************************************
        var phone = document.getElementById('telephone'),
                alertphone = document.getElementById('alerttelephone');

        phone.addEventListener('focus', function(){     // on se rassure que les erreurs ne s'affichent pas lorsqu'on est dans le champ.
                alertphone.style.opacity='0';
        });
                
        phone.addEventListener('blur', testtelephone);
        function testphone(){
                if (phone.value%1 != 0) {  // on se rassure que le champ ne contient que des chiffres.
                        alertphone.innerHTML = "Ce champ doit contenir au moins 8 chiffres sans espace ni caracteres tels que '@', '%'' ou '-'";
                        alertphone.style.opacity = '1';
                        return false;
                }else{
                        return true;
                }
                
        }

// traitement de l'adresse mail *****************************************************************
        var email = document.getElementById('email');
                alertemail = document.getElementById('alertemail');

        email.addEventListener('focus', function(){      // on se rassure que les erreurs ne s'affichent pas lorsqu'on est dans le champ.
                alertemail.style.opacity='0';
        });

        mail.addEventListener('blur', testemail);
        function testmail(){
                var email_indic='0';
                var email_format=[],email_ending=[];
                email_format=['gmail.com','yahoo.com','yahoo.fr','hotmail.com']; // les formats de mail acceptes

                for(var i=0; i<email.value.length; i++){  // onparcours l'adresse mail saisie pour retrouver le caractere @
                        if(email.value[i]=='@'){         // si @ est reouve on le garde comme indicateur
                                email_indic=email.value[i];
                        }
                }
                
                if(email_indic=='@'){         
                        email_ending=email.value.split('@');   //si on a notre indicateur on l'utilise pour separer l'adresse entree en 2 blocks
                        
                        for (var i = 0; i < email_format.length; i++) {  // on parcours le tableau des formats de mail.
                                console.log(email_format[i]);
                                console.log(email_format.length);
                                if(mail_format[i]==email_ending[1]){             // on verifie si la partie entree en adresse apres le @ vaut bien l'une des valeurs souhaitee
                                        return true;
                                }
                                else if(i==email_format.length-1 && email_format[i]!=email_ending[1]){   // si jusqu'au dernier element des formats on ne trouve rien alors erreur
                                        alertemail.innerHTML = "Le format de mail attendu est 'exemple@xmail.xxx";
                                        alertemail.style.opacity='1';
                                        return false;
                                }
                        }
                }
                else{           // si on n'a pas l'indicateur, l'adresse mail n'est pas valide
                        alertemail.innerHTML = "Le format de mail attendu est 'exemple@xmail.xxx";
                        alertemail.style.opacity = '1';
                        return false;
                }
        }

        //traitement du submit ***************************************************************
        var envoie = document.getElementById('submit'),
                formulaire = document.getElementById('formulaire'),
                alert = document.getElementById('alertgeneral');
                envoie.addEventListener('click', testchamp);
        function testchamp(){
                if(!testnom()){
                        alertnom.style.opacity = '1';
                        alert.style.opacity = '1';
                }else if(!testprenom()){
                        alertprenom.style.opacity = '1';
                        alert.style.opacity = '1';
                }else if(!testmail()){
                        alertmail.style.opacity = '1';
                        alert.style.opacity = '1';
                }
                // else{
                //      formulaire.submit();
                // }
        }






var nom=document.getElementsByTagName('input')[0];
var prenom=document.getElementsByTagName('input')[1];
var email=document.getElementsByTagName('input')[2];
var nom_utilisateur= document.getElementsByTagName('input')[3];
var telephone= document.getElementsByTagName('input')[4];


var p_alert=document.getElementsByTagName('p')[0];  
var p_nom=document.getElementsByTagName('p')[1];
var p_prenom=document.getElementsByTagName('p')[2];
var p_email=document.getElementsByTagName('p')[3];
var p_nom_utilisateur= document.getElementsByTagName('input')[4];
var p_telephone= document.getElementsByTagName('input')[5];
                
var submit=document.getElementById('submit');
var reset=document.getElementById('reset');
var formulaire=document.getElementById('formulaire');


        function contient_specialchart(char){
                var special = ['@','!','~','<','>','?',' " '," ' ",'*','(',')','^','%','$','#','&','{','}','[',']',';',];

                for (var i = special.length - 1; i >= 0; i--) { //pour chaque element de special
                        for (var id in char) {  // on se rassure qu'il ne soit pas dans char
                                if(char[id]==special[i]){
                                        return true;
                                }
                        }
                }
                return false;
        }


        nom.addEventListener('change', testnom);
        function testnom(){
                // console.log(nom.value);  //verification du fonctionnement
                if(prenom.value.length<3){  //test de taille de contenu
                        alertnom.innerHTML = "Le nom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
                        alertnom.style.opacity='1';   //penser a le faire avec une duree definie. 
                        return false;
                }
                else if(contient_specialchart(prenom.value)){  // test de caracteres non indiques
                        alertnom.innerHTML = "Le nom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
                        alertnom.style.opacity = '1';
                        return false;
                }else{
                        return true;
                }
        }

        prenom.addEventListener('change', testprenom);
        // console.log(prenom);
        function testprenom(){
                // console.log(prenom.value);  //verification du fonctionnement
                if(prenom.value.length<3){  //test de taille de contenu
                        p_prenom.style.display='block'; 
                        return false;
                }
                else if(contient_specialchart(prenom.value)){  // test de caracteres non indiques
                        p_prenom.style.display='block';
                        return false;
                }else{
                        p_prenom.style.display='none';
                        nom.style.backgroundColor='rgb(232,240,254)';
                        return true;;
                }

        nom_utilisateur.addEventListener('change', testnom_utilisateur)
        function testnom_utilisateur(){
                // console.log(nom_utilisateur.value);  //verification du fonctionnement
                if(nom_utilisateur.value.length<3){  //test de taille de contenu
                        p_nom_utilisateur.style.display='block'; 
                        return false;
                }
                else if(contient_specialchart(nom_utilisateur.value)){  // test de caracteres non indiques
                        p_nom_utilisateur.style.display='block';
                        return false;
                }else{
                        p_nom_utilisateur.style.display='none';
                        nom.style.backgroundColor='rgb(232,240,254)';
                        return true;
                }

        telephone.addEventListener('change', testtelephone);
        function testtelephone(){
                if (telephone.value%1 != 0) {  // on se rassure que le champ ne contient que des chiffres.
                        p_telephone.style.display='block';
                        return false;
                }else{
                        p_telephone.style.display='none';
                        nom.style.backgroundColor='rgb(232,240,254)';
                        return true;
                }
                
        }

        email.addEventListener('change', testemail);
        // console.log(email);
        function testemail(){
                        var email_indic='0';
                        var email_format=[],email_ending=[];
                        email_format=['gmail.com','yahoo.com','yahoo.fr','hotmail.com'];
                        var email_ok;
                        // console.log('test');
                        for(var i=0; i<email.value.length; i++){
                                if(email.value[i]=='@'){
                                        email_indic=email.value[i];
                                }
                        }
                        // console.log(email_indic);
                        if(email_indic=='@'){
                                email_ending=email.value.split('@');
                                // console.log(email_ending);
                                for (var i = 0; i < email_format.length; i++) {
                                        // console.log(email_format[i]+' ?= '+email_ending[1]);
                                        if(email_format[i]==email_ending[1]){
                                                email_ok=true;
                                                break;
                                        }
                                        else{
                                                email_ok=false;
                                        }
                                }
                        }
                        else{
                                email_ok=false;
                        }

                        if(email_ok==true){
                                p_email.style.display='none';
                                email.style.backgroundColor='rgb(232,240,254)';
                                return true;
                        }
                        else{
                                p_email.style.display='block';
                                return false;
                        }
                }

        submit.addEventListener('click', function(){
                var check=[];

                        if(testnom()){
                                check[0]=true;
                        }
                        else{
                                check[0]=false;
                        }

                        if(testprenom()){
                                check[1]=true;
                        }
                        else{
                                check[1]=false;
                        }

                        if(testemail()){
                                check[2]=true;
                        }
                        else{
                                check[2]=false;
                        }

                        if(testnom_utilisateur()){
                                check[3]=true;
                        }
                        else{
                                check[3]=false;
                        }

                        if(testtelephone()){
                                check[4]=true;
                        }
                        else{
                                check[4]=false;
                        }

                        if(check[0]&&check[1]&&check[2]&&check[3]&&check[4]){
                                formulaire.submit();
                        }
                        else{
                                p_alert.style.display='block';
                        }
        });     

        reset.addEventListener('click', function(){
                p_alert.style.display='none';
                p_nom.style.display='none';
                p_prenom.style.display='none';
                p_mail.style.display='none';
                p_nom_utlisateur.style.display='none';
                p_telephone.style.display='none';
                formulaire.reset();

        });